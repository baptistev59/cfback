package fr.azzahrah.cfback.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import fr.azzahrah.cfback.beans.Users;
import fr.azzahrah.cfback.idao.IUsersDao;
import fr.azzahrah.cfback.iservices.IUsersServices;

@Service
@Qualifier("services")
@Repository
public class UsersServices implements IUsersServices{
	
	@Autowired
	IUsersDao iUsersDao;

	@Override
	public Users findbyUsernameAndPassword(String Username,String Password) {
		
		return iUsersDao.findbyUsernameAndPassword(Username,Password);
	}
	
	public List<Users> findAll(){
		return iUsersDao.findAll();
	}

}
