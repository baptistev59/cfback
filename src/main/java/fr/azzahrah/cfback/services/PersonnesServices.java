package fr.azzahrah.cfback.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import fr.azzahrah.cfback.beans.Personnes;
import fr.azzahrah.cfback.idao.IPersonnesDao;
import fr.azzahrah.cfback.iservices.IPersonnesServices;

@Service
@Qualifier("services")
@Repository
public class PersonnesServices implements IPersonnesServices {

	@Autowired
	IPersonnesDao iPersonneDao;

	public List<String> nomPersAllPers() {

		List<String> listNomPersAllPer = new ArrayList<>();
		List<Personnes> listePersonnes = iPersonneDao.findAll();

		for (Personnes personne : listePersonnes) {
			listNomPersAllPer.add(personne.getNomPers() + " " + personne.getPrenomPers() + " " + personne.getMailPers()
					+ " " + personne.getIdDossPers());
		}
		return listNomPersAllPer;
	}

	public Personnes findByDataPers(String dataPers) {
		
		String[] tabDataPers = dataPers.split(" ");

		String nomPers = tabDataPers[0];
		String prenomPers = tabDataPers[1];
		String mailPers = tabDataPers[2];
		String idDossPers = tabDataPers[3];

		mailPers = mailPers.replaceAll("\\%40", "\\@");

		return iPersonneDao.findByData(nomPers, prenomPers, mailPers, idDossPers);
		
	}

	public Personnes deleteByDataPers(String dataPers) {

		String[] tabDataPers = dataPers.split(" ");

		String nomPers = tabDataPers[0];
		String prenomPers = tabDataPers[1];
		String mailPers = tabDataPers[2];
		String idDossPers = tabDataPers[3];

		mailPers = mailPers.replaceAll("\\%40", "\\@");

		return iPersonneDao.deleteByData(nomPers, prenomPers, mailPers, idDossPers);

	}
	
	
}
