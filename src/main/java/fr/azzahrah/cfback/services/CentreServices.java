package fr.azzahrah.cfback.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import fr.azzahrah.cfback.beans.Centre;
import fr.azzahrah.cfback.idao.ICentreDao;
import fr.azzahrah.cfback.iservices.ICentreServices;

@Service
@Qualifier("services")
@Repository
public class CentreServices implements ICentreServices{
	
	@Autowired
	ICentreDao iCentreDao;
	
	@Override
	public List<Centre> findAll(){
		return iCentreDao.findAll();
		
	}
	
	@Override
	public Centre findById(Long id) {
		return iCentreDao.findById(id).get();
		
	};
	
	@Override
	public Centre save(Centre centre) {
		return iCentreDao.save(centre);
	}
	
	@Override
	public void deleteById(Long id) {
		iCentreDao.deleteById(id);
	}

}
