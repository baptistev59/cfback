package fr.azzahrah.cfback.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import fr.azzahrah.cfback.beans.Categories;
import fr.azzahrah.cfback.beans.Formations;
import fr.azzahrah.cfback.beans.Sessions;
import fr.azzahrah.cfback.idao.IFormationsDao;
import fr.azzahrah.cfback.iservices.IFormationsServices;
import fr.azzahrah.cfback.iservices.ISessionsServices;

@Service
@Qualifier("services")
@Repository
public class FormationsServices implements IFormationsServices {

	@Autowired
	ISessionsServices iSessionServices;

	@Autowired
	IFormationsDao iFormationDao;


	@Override
	public List<Formations> findByCategorie(Categories categorie) {

		return iFormationDao.findByCategorie(categorie);
	}

	public List<Formations> findAll(){
		return iFormationDao.findAll();
		
	}
	
	public Formations findById(Long id) {
		return iFormationDao.findById(id).get();
		
	};
	
	public Formations save(Formations formation) {
		return iFormationDao.save(formation);
	}
	
	public void deleteById(Long id) {
		iFormationDao.deleteById(id);
	}

	@Override
	public Formations findBySession(Sessions session) {
		// TODO Auto-generated method stub
		return null;
	}
}
