package fr.azzahrah.cfback.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import fr.azzahrah.cfback.beans.Personnes;
import fr.azzahrah.cfback.beans.Sessions;
import fr.azzahrah.cfback.idao.IPersonnesDao;
import fr.azzahrah.cfback.idao.ISessionsDao;
import fr.azzahrah.cfback.iservices.ISessionsServices;

@Service
@Qualifier("sessions")
@Repository
public class SessionsServices implements ISessionsServices {

	@Autowired
	ISessionsDao iSessionDao;
	@Autowired
	IPersonnesDao iPersonneDao;

	public List<Sessions> listAnimationsSessionsByPers(Long id) {

		List<Sessions> listSessions = new ArrayList<>();
		Personnes formateurs = iPersonneDao.findById(id).get();
		listSessions = formateurs.getListAnimationsSessions();
		return listSessions;
	}
	
	public List<Sessions> findAll(){
		return iSessionDao.findAll();
	}

	public List<Sessions> listParticipationsSessionsByPers(Long id) {

		List<Sessions> listSessions = new ArrayList<>();
		Personnes stagiaire = iPersonneDao.findById(id).get();
		listSessions = stagiaire.getListParticipationsSessions();
		return listSessions;
	}

	@Override
	public List<String> libelleSessionAllSession() {
		List<String> listLibelleSessionAllSession = new ArrayList<>();
		List<Sessions> listeSession = iSessionDao.findAll();

		for (Sessions session : listeSession) {
			listLibelleSessionAllSession.add(session.getLibelleSession());
		}
		return listLibelleSessionAllSession;
	}

	@Override
	public Sessions findByDataSession(String data) {

		return iSessionDao.findByLibelleSession(data);
	}

	@Override
	public Sessions findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

}
