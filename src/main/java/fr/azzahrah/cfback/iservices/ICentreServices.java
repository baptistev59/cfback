package fr.azzahrah.cfback.iservices;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Centre;

@Repository
public interface ICentreServices {
	
	public List<Centre> findAll();

	public Centre findById(Long id);

	public Centre save(Centre centre);

	public void deleteById(Long id);

}
