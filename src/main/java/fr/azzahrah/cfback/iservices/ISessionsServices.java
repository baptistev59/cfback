package fr.azzahrah.cfback.iservices;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Sessions;

@Repository
public interface ISessionsServices {

	public List<Sessions> listAnimationsSessionsByPers(Long id);

	public List<Sessions> listParticipationsSessionsByPers(Long id);
	
	public List<String> libelleSessionAllSession();

	public Sessions findByDataSession(String data);
	
	public List<Sessions> findAll();
	
	public Sessions findById(Long id);

}
