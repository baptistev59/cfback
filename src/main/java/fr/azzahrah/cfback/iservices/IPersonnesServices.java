package fr.azzahrah.cfback.iservices;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Personnes;

@Repository
public interface IPersonnesServices {
	
	public List<String> nomPersAllPers();
	
	public Personnes findByDataPers(String dataPers);
	
	public Personnes deleteByDataPers(String dataPers);

	
}
