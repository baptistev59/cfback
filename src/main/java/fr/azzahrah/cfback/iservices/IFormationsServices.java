package fr.azzahrah.cfback.iservices;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Categories;
import fr.azzahrah.cfback.beans.Formations;
import fr.azzahrah.cfback.beans.Sessions;

@Repository
public interface IFormationsServices {
	
	public Formations findBySession(Sessions session);

	public List<Formations> findByCategorie(Categories categorie);

	public List<Formations> findAll();

	public Formations findById(Long id);

	public Formations save(Formations formation);

	public void deleteById(Long id);

}
