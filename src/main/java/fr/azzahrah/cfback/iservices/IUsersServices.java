package fr.azzahrah.cfback.iservices;

import java.util.List;

import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Users;

@Repository
public interface IUsersServices {

	Users findbyUsernameAndPassword(String Username,String Password);

	List<Users> findAll();

}
