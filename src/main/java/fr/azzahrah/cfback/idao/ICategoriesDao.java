package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Categories;

@Repository
public interface ICategoriesDao extends JpaRepository<Categories, Long> {

}
