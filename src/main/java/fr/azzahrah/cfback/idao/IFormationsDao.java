package fr.azzahrah.cfback.idao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Categories;
import fr.azzahrah.cfback.beans.Formations;

@Repository
public interface IFormationsDao extends JpaRepository<Formations, Long>{

	List<Formations> findByCategorie(Categories categorie);


}
