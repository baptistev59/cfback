package fr.azzahrah.cfback.idao;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Roles;

@Repository
public interface IRolesDao extends JpaRepository<Roles, Long>{

	Optional<Roles> findByLibeleRole(String lib);
	
	

}
