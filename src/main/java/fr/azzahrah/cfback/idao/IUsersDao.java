package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import fr.azzahrah.cfback.beans.Users;

public interface IUsersDao extends JpaRepository<Users, Long> {

	@Query("select u from Users u where u.username=?1 and u.password=?2")
	Users findbyUsernameAndPassword(String Username, String Password);

}
