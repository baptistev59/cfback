package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Competences;

@Repository
public interface ICompetencesDao extends JpaRepository<Competences, Long> {

}
