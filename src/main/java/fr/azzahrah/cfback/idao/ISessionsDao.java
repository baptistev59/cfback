package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Sessions;

@Repository
public interface ISessionsDao extends JpaRepository<Sessions, Long> {

	Sessions findByLibelleSession(String data);



}
