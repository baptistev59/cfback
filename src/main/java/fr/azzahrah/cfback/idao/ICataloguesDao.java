package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Catalogues;

@Repository
public interface ICataloguesDao extends JpaRepository<Catalogues, Long>{

}
