package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Presences;

@Repository
public interface IPresencesDao extends JpaRepository<Presences, Long>{

}
