package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Adresses;

@Repository
public interface IAdressesDao extends JpaRepository<Adresses, Long>{

	Adresses findByLabel(String label);
}
