package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Centre;

@Repository
public interface ICentreDao extends JpaRepository<Centre, Long>{

}
