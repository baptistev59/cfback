package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import fr.azzahrah.cfback.beans.Personnes;

@Repository
public interface IPersonnesDao extends JpaRepository<Personnes, Long> {

	Personnes findByNomPers(String nomPers);

	@Transactional
	Integer deleteByNomPers(String nomPers);

	@Query("select p from Personnes p where p.nomPers=?1 and p.prenomPers=?2 and p.mailPers=?3 and p.idDossPers=?4")
	Personnes findByData(String nomPers, String prenomPers, String mailPers, String idDossPers);

	@Query("delete from Personnes p where p.nomPers=?1 and p.prenomPers=?2 and p.mailPers=?3 and p.idDossPers=?4")
	Personnes deleteByData(String nomPers, String prenomPers, String mailPers, String idDossPers);
}
