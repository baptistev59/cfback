package fr.azzahrah.cfback.idao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Sessions;
import fr.azzahrah.cfback.beans.UnitesFormation;

@Repository
public interface IUnitesFormationDao extends JpaRepository<UnitesFormation, Long> {

	void deleteByNumUF(String numUF);
	
	public List<UnitesFormation> findBySession(Sessions session);

}
