package fr.azzahrah.cfback.idao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import fr.azzahrah.cfback.beans.Lieux;

@Repository
public interface ILieuxDao extends JpaRepository<Lieux, Long>{

}
