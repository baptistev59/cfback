package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.UnitesFormation;
import fr.azzahrah.cfback.idao.IUnitesFormationDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Unités de formation.")
public class UnitesFormationController {
	
	@Autowired
	IUnitesFormationDao iUnitesFormationDao;
	
	@ApiOperation(value = "Récupère la liste des unités de formation.")
	@RequestMapping(value = "/UnitesFormation", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeUnitesFormation() {

		List<UnitesFormation> listeUnitesFormation = iUnitesFormationDao.findAll();
		MappingJacksonValue mjvListeUnitesFormation = new MappingJacksonValue(listeUnitesFormation);

		if (listeUnitesFormation.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeUnitesFormation);
		}
	}
	
	@ApiOperation(value = "Récupère une unité de formation via son ID.")
	@RequestMapping(value = "/UnitesFormation/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUneUniteFormation(@PathVariable Long id) {

		UnitesFormation uniteFormationFind = iUnitesFormationDao.findById(id).get();
		MappingJacksonValue mjvUniteFormation = new MappingJacksonValue(uniteFormationFind);

		if (uniteFormationFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvUniteFormation);
		}
	}
	
	@ApiOperation(value = "Ajoute une unité de formation.")
//	@ApiIgnore
	@PostMapping(value = "/UnitesFormation")
	public ResponseEntity<Object> ajouterUniteFormation(@RequestBody UnitesFormation uniteFormation) {
		UnitesFormation uniteFormationAdd = iUnitesFormationDao.save(uniteFormation);

		if (uniteFormationAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(uniteFormationAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime une unité de formation.")
//	@ApiIgnore
	@RequestMapping(value="/UnitesFormation/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerUnitesFormation(@PathVariable Long id) throws NotFoundException {
		
		iUnitesFormationDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
