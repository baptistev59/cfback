package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Competences;
import fr.azzahrah.cfback.idao.ICompetencesDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Compétences.")
public class CompetencesController {

	@Autowired
	ICompetencesDao iCompetenceDao;
	
	@ApiOperation(value = "Récupère la liste des compétences.")
	@RequestMapping(value = "/Competences", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeCompetences() {

		List<Competences> listeCompetences = iCompetenceDao.findAll();
		MappingJacksonValue mjvListeCompetences = new MappingJacksonValue(listeCompetences);

		if (listeCompetences.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeCompetences);
		}
	}
	
	@ApiOperation(value = "Récupère une compétence via son ID.")
	@RequestMapping(value = "/Competences/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUneCompetence(@PathVariable Long id) {

		Competences CompetencesFind = iCompetenceDao.findById(id).get();
		MappingJacksonValue mjvCompetences = new MappingJacksonValue(CompetencesFind);

		if (CompetencesFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvCompetences);
		}
	}
	
	@ApiOperation(value = "Ajoute une compétence.")
//	@ApiIgnore
	@PostMapping(value = "/Competences")
	public ResponseEntity<Object> ajouterCompetences(@RequestBody Competences competence) {
		Competences competenceAdd = iCompetenceDao.save(competence);

		if (competenceAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(competenceAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime une compétence via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Competences/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerCompetences(@PathVariable Long id) throws NotFoundException {
		
		iCompetenceDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
