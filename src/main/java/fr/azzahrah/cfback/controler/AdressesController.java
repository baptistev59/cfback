package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Adresses;
import fr.azzahrah.cfback.idao.IAdressesDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Adresses.")
public class AdressesController {
	
	@Autowired
	IAdressesDao iAdresseDao;
	
	@ApiOperation(value = "Récupère la liste des adresses.")
	@RequestMapping(value = "/Adresses", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeAdresses() {

		List<Adresses> listeAdresses = iAdresseDao.findAll();
		MappingJacksonValue mjvListeAdresses = new MappingJacksonValue(listeAdresses);

		if (listeAdresses.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeAdresses);
		}
	}
	
	@ApiOperation(value = "Récupère une adresse via son ID.")
	@RequestMapping(value = "/Adresses/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUneAdresse(@PathVariable Long id) {

		Adresses adresseFind = iAdresseDao.findById(id).get();
		MappingJacksonValue mjvAdresse = new MappingJacksonValue(adresseFind);

		if (adresseFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvAdresse);
		}
	}

	@ApiOperation(value = "Ajoute une adresse.")
//	@ApiIgnore
	@PostMapping(value = "/Adresses")
	public ResponseEntity<Object> ajouterAdresse(@RequestBody Adresses adresse) {
		Adresses adresseAdd = iAdresseDao.save(adresse);

		if (adresseAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(adresseAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime une adresse via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Adresses/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerAdresses(@PathVariable Long id) throws NotFoundException {
		
		iAdresseDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
