package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Adresses;
import fr.azzahrah.cfback.beans.Lieux;
import fr.azzahrah.cfback.idao.IAdressesDao;
import fr.azzahrah.cfback.idao.ILieuxDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Lieux.")
public class LieuxController {
	
	@Autowired
	ILieuxDao iLieuDao;
	
	@Autowired
	IAdressesDao iAdresseDao;
	
	@ApiOperation(value = "Récupère la liste des lieux.")
	@RequestMapping(value = "/Lieux", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeLieux() {

		List<Lieux> listeLieux = iLieuDao.findAll();
		MappingJacksonValue mjvListeLieux = new MappingJacksonValue(listeLieux);

		if (listeLieux.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeLieux);
		}
	}
	
	@ApiOperation(value = "Récupère un lieu via son ID.")
	@RequestMapping(value = "/Lieux/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnLieu(@PathVariable Long id) {

		Lieux lieuFind = iLieuDao.findById(id).get();
		MappingJacksonValue mjvLieux = new MappingJacksonValue(lieuFind);

		if (lieuFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvLieux);
		}
	}
	
	@ApiOperation(value = "Ajoute un lieu.")
//	@ApiIgnore
	@PostMapping(value = "/Lieux/addLieu")
	public ResponseEntity<Object> ajouterLieu(@RequestBody Lieux lieu) {
		Adresses adresse = new Adresses();
		
		if (iAdresseDao.findByLabel(lieu.getAdresse().getLabel()) == null) {
			adresse = iAdresseDao.save(lieu.getAdresse());
		}else {
			adresse = iAdresseDao.findByLabel(lieu.getAdresse().getLabel());
		}
		
		lieu.setAdresse(adresse);
		Lieux lieuAdd = iLieuDao.save(lieu);

		if (lieuAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(lieuAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime un lieu via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Lieux/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerLieux(@PathVariable Long id) throws NotFoundException {
		
		iLieuDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}

}
