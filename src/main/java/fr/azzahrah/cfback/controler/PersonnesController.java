package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Personnes;
import fr.azzahrah.cfback.idao.IPersonnesDao;
import fr.azzahrah.cfback.iservices.IPersonnesServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Personnes.")
public class PersonnesController {

	@Autowired
	private IPersonnesDao iPersonneDao;

	@Autowired
	private IPersonnesServices iPersonneServices;

	@ApiOperation(value = "Récupère la liste des Personnes.")
	@RequestMapping(value = "/Personnes", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListePersonnes() {

		List<Personnes> listePersonnes = iPersonneDao.findAll();
		MappingJacksonValue mjvListePersonnes = new MappingJacksonValue(listePersonnes);

		if (listePersonnes.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListePersonnes);
		}
	}

	@ApiOperation(value = "Récupère une Personne via son ID.")
	@RequestMapping(value = "/Personnes/findid={id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnePersonne(@PathVariable Long id) {

		Personnes personneFind = iPersonneDao.findById(id).get();
		MappingJacksonValue mjvPersonne = new MappingJacksonValue(personneFind);

		if (personneFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvPersonne);
		}
	}

	@ApiOperation(value = "Récupère une Personne via un string \"nom, prénom, mail, id du dossier\".")
	@RequestMapping(value = "/Personnes/finddata={data}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnePersonneByData(@PathVariable String data) {

		Personnes personneFind = iPersonneServices.findByDataPers(data);
		MappingJacksonValue mjvPersonne = new MappingJacksonValue(personneFind);

		if (personneFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvPersonne);
		}
	}

	@ApiOperation(value = "Récupère une Personne via un string \"nom, prénom, mail, id du dossier\".")
	@RequestMapping(value = "/Personnes/finddatap", method = RequestMethod.POST)
	public ResponseEntity<Object> afficherUnePersonneByDataP(@RequestBody String data) {

		Personnes personneFind = iPersonneServices.findByDataPers(data);
		MappingJacksonValue mjvPersonne = new MappingJacksonValue(personneFind);

		if (personneFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvPersonne);
		}
	}
	@ApiOperation(value = "Récupère une Personne via son nom")
	@RequestMapping(value = "/Personnes/findnom={nomPers}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnePersonneParNom(@PathVariable String nomPers) {

		Personnes personneFind = iPersonneDao.findByNomPers(nomPers);
		MappingJacksonValue mjvPersonne = new MappingJacksonValue(personneFind);

		if (personneFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvPersonne);
		}
	}

	@ApiOperation(value = "Ajoute une Personne.")
//	@ApiIgnore
	@PostMapping(value = "/Personnes")
	public ResponseEntity<Object> addPersonne(@RequestBody Personnes personne) {
		Personnes p1 = personne;

		Personnes personneFind = iPersonneDao.findByData(p1.getNomPers(), p1.getPrenomPers(), p1.getMailPers(),
				p1.getIdDossPers());

		if (personneFind != null) {
			return ResponseEntity.noContent().build();
		} else {
			Personnes personneAdd = iPersonneDao.save(personne);
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
					.buildAndExpand(personneAdd.getId()).toUri();
			return ResponseEntity.created(location).build();
		}

	}

	@ApiOperation(value = "Supprime une Personne via son ID.")
//	@ApiIgnore
	@RequestMapping(value = "/Personnes/delid={id}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> delPersonnes(@PathVariable Long id) throws NotFoundException {

		iPersonneDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Supprime une Personne via un string \"nom, prénom, mail, id du dossier\".")
//	@ApiIgnore
	@RequestMapping(value = "/Personnes/deldatap", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteUnePersonneByDataP(@RequestBody String data) {

		iPersonneServices.deleteByDataPers(data);
		return ResponseEntity.noContent().build();
	}
	@ApiOperation(value = "Supprime une Personne via son nom.")
//	@ApiIgnore
	@RequestMapping(value = "/Personnes/delnom={nomPers}", method = RequestMethod.DELETE)
	public ResponseEntity<Void> deleteByNomPers(@PathVariable String nomPers) throws NotFoundException {

		iPersonneDao.deleteByNomPers(nomPers);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Récupère une liste des noms des personnes.")
	@RequestMapping(value = "/Personnes/listNomPers", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeNomPers() {

		List<String> listeNomPers = iPersonneServices.nomPersAllPers();
		MappingJacksonValue mjvListeNomPers = new MappingJacksonValue(listeNomPers);

		if (listeNomPers.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeNomPers);
		}
	}
}
