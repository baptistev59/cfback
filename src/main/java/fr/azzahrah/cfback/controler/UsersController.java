package fr.azzahrah.cfback.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.azzahrah.cfback.beans.Users;
import fr.azzahrah.cfback.iservices.IUsersServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Users.")
public class UsersController {

	@Autowired
	private IUsersServices iUsersService;
	
	@ApiOperation(value = "Récupère la liste des users.")
	@RequestMapping(value = "/Users", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeFormations() {

		List<Users> listeUsers = iUsersService.findAll();
		MappingJacksonValue mjvListeUsers = new MappingJacksonValue(listeUsers);

		if (listeUsers.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeUsers);
		}
	}
	
	@ApiOperation(value = "Récupère un user via le username et password.")
	@RequestMapping(value = "/Users/find", method = RequestMethod.POST)
	public ResponseEntity<Object> find(@RequestBody Users user) {

		Users userFind = iUsersService.findbyUsernameAndPassword(user.getUsername(),user.getPassword());
		MappingJacksonValue mjvUserFind = new MappingJacksonValue(userFind);

		if (userFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvUserFind);
		}
	}
}
