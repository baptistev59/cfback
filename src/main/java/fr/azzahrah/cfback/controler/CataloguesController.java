package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Catalogues;
import fr.azzahrah.cfback.idao.ICataloguesDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Catalogues.")
public class CataloguesController {

	@Autowired
	ICataloguesDao iCatalogueDao;
	
	@ApiOperation(value = "Récupère la liste des catalogues.")
	@RequestMapping(value = "/Catalogues", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeCatalogues() {

		List<Catalogues> listeCatalogues = iCatalogueDao.findAll();
		MappingJacksonValue mjvListeCatalogues = new MappingJacksonValue(listeCatalogues);

		if (listeCatalogues.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeCatalogues);
		}

	}
	@ApiOperation(value = "Récupère un catalogue via son ID.")
	@RequestMapping(value = "/Catalogues/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnCatalogue(@PathVariable Long id) {

		Catalogues catalogueFind = iCatalogueDao.findById(id).get();
		MappingJacksonValue mjvCatalogue = new MappingJacksonValue(catalogueFind);

		if (catalogueFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvCatalogue);
		}
	}

	@ApiOperation(value = "Ajoute un catalogue.")
//	@ApiIgnore
	@PostMapping(value = "/Catalogues")
	public ResponseEntity<Object> ajouterCatalogue(@RequestBody Catalogues catalogue) {
		Catalogues catalogueAdd = iCatalogueDao.save(catalogue);

		if (catalogueAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(catalogueAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime un catalogues via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Catalogues/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerCatalogues(@PathVariable Long id) throws NotFoundException {
		
		iCatalogueDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}
}
