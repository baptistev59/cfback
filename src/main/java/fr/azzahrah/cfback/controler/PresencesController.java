package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Presences;
import fr.azzahrah.cfback.idao.IPresencesDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import springfox.documentation.annotations.ApiIgnore;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Présences.")
public class PresencesController {

	@Autowired
	IPresencesDao iPresenceDao;
	
	@ApiOperation(value = "Récupère la liste des présences.")
	@RequestMapping(value = "/Presences", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListePresences() {

		List<Presences> listePresences = iPresenceDao.findAll();
		MappingJacksonValue mjvListePresences = new MappingJacksonValue(listePresences);

		if (listePresences.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListePresences);
		}
	}
	
	@ApiOperation(value = "Récupère une présence via son ID.")
	@RequestMapping(value = "/Presences/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnePresence(@PathVariable Long id) {

		Presences presenceFind = iPresenceDao.findById(id).get();
		MappingJacksonValue mjvPresences = new MappingJacksonValue(presenceFind);

		if (presenceFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvPresences);
		}
	}
	
	@ApiOperation(value = "Ajoute une présence.")
	@ApiIgnore
	@PostMapping(value = "/Presences")
	public ResponseEntity<Object> ajouterCatalogue(@RequestBody Presences presence) {
		Presences presenceAdd = iPresenceDao.save(presence);

		if (presenceAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(presenceAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime une présence via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Presences/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerPresence(@PathVariable Long id) throws NotFoundException {
		
		iPresenceDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
}
