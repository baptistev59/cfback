package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Roles;
import fr.azzahrah.cfback.idao.IRolesDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Rôles.")
public class RolesController {

	@Autowired
	IRolesDao iRoleDao;
	
	@ApiOperation(value = "Récupère la liste des rôles.")
	@RequestMapping(value = "/Roles", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeRoles() {

		List<Roles> listeRoles = iRoleDao.findAll();
		MappingJacksonValue mjvListeRoles = new MappingJacksonValue(listeRoles);

		if (listeRoles.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeRoles);
		}
	}
	
	@ApiOperation(value = "Récupère un rôle via son ID.")
	@RequestMapping(value = "/Roles/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnRole(@PathVariable Long id) {

		Roles roleFind = iRoleDao.findById(id).get();
		MappingJacksonValue mjvRole = new MappingJacksonValue(roleFind);

		if (roleFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvRole);
		}
	}
	
	@ApiOperation(value = "Récupère un rôle via son libellé.")
	@RequestMapping(value = "/Roles/lib={lib}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnRole(@PathVariable String lib) {

		Roles roleFind = iRoleDao.findByLibeleRole(lib).get();
		MappingJacksonValue mjvRole = new MappingJacksonValue(roleFind);

		if (roleFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvRole);
		}
	}
	
	@ApiOperation(value = "Ajoute un rôle.")
//	@ApiIgnore
	@PostMapping(value = "/Roles")
	public ResponseEntity<Object> ajouterRoles(@RequestBody Roles role) {
		Roles rolesAdd = iRoleDao.save(role);

		if (rolesAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(rolesAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime un rôle via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Roles/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerRoles(@PathVariable Long id) throws NotFoundException {
		
		iRoleDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
}
