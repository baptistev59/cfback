package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Centre;
import fr.azzahrah.cfback.iservices.ICentreServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Centres.")
public class CentreController {

	@Autowired
	private ICentreServices iCentreServices;
	
	@ApiOperation(value = "Récupère la liste des Centres.")
	@RequestMapping(value = "/Centre", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeCentres() {

		List<Centre> listeCentres = iCentreServices.findAll();
		MappingJacksonValue mjvListeCentres = new MappingJacksonValue(listeCentres);

		if (listeCentres.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeCentres);
		}
	}
	
	@ApiOperation(value = "Récupère un Centre via son ID.")
	@RequestMapping(value = "/Centre/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUnCentre(@PathVariable Long id) {

		Centre centreFind = iCentreServices.findById(id);
		MappingJacksonValue mjvCentre = new MappingJacksonValue(centreFind);

		if (centreFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvCentre);
		}
		
		
	}
	
	@ApiOperation(value = "Ajoute un Centre.")
//	@ApiIgnore
	@PostMapping(value = "/Centre")
	public ResponseEntity<Object> ajouterCentre(@RequestBody Centre centre) {
		Centre centreAdd = iCentreServices.save(centre);

		if (centreAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(centreAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime un centre via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Centre/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerCentre(@PathVariable Long id) throws NotFoundException {
		
		iCentreServices.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
}
