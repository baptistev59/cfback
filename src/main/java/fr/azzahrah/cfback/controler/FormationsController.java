package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Categories;
import fr.azzahrah.cfback.beans.Formations;
import fr.azzahrah.cfback.beans.Sessions;
import fr.azzahrah.cfback.iservices.IFormationsServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Formations.")
public class FormationsController {
	
	@Autowired
	private IFormationsServices iFormationsService;
	
	@ApiOperation(value = "Récupère la liste des formations.")
	@RequestMapping(value = "/Formations", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeFormations() {

		List<Formations> listeFormations = iFormationsService.findAll();
		MappingJacksonValue mjvListeFormations = new MappingJacksonValue(listeFormations);

		if (listeFormations.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeFormations);
		}
	}
	
	@ApiOperation(value = "Récupère une formation via son ID.")
	@RequestMapping(value = "/Formations/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUneFormation(@PathVariable Long id) {

		Formations FormationFind = iFormationsService.findById(id);
		MappingJacksonValue mjvFormations = new MappingJacksonValue(FormationFind);

		if (FormationFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvFormations);
		}
	}
	
	@ApiOperation(value = "Ajoute une formation.")
//	@ApiIgnore
	@PostMapping(value = "/Formations")
	public ResponseEntity<Object> ajouterFormation(@RequestBody Formations formation) {
		Formations formationAdd = iFormationsService.save(formation);

		if (formationAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(formationAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime une formation via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Formations/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerFormations(@PathVariable Long id) throws NotFoundException {
		
		iFormationsService.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	@ApiOperation(value = "Récupère une formation via une session.")
	@RequestMapping(value = "/Formations/findbysession", method = RequestMethod.POST)
	public ResponseEntity<Object> afficherUneFormationsBySession(@RequestBody Sessions session) {

		Formations formationFind = iFormationsService.findBySession(session);
		MappingJacksonValue mjvFormation = new MappingJacksonValue(formationFind);

		if (formationFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvFormation);
		}
	}
	
	@ApiOperation(value = "Récupère une formation via une catégorie.")
	@RequestMapping(value = "/Formations/findbycategorie", method = RequestMethod.POST)
	public ResponseEntity<Object> afficherUneFormationsByCategorie(@RequestBody Categories categorie) {

		List<Formations> listFormations = iFormationsService.findByCategorie(categorie);
		MappingJacksonValue mjvListFormations = new MappingJacksonValue(listFormations);

		if (listFormations == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListFormations);
		}
	}
}
