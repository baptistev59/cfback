package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Sessions;
import fr.azzahrah.cfback.beans.UnitesFormation;
import fr.azzahrah.cfback.idao.ISessionsDao;
import fr.azzahrah.cfback.idao.IUnitesFormationDao;
import fr.azzahrah.cfback.iservices.ISessionsServices;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Sessions.")
public class SessionsController {
	
	@Autowired
	ISessionsDao iSessionDao;
	
	@Autowired
	IUnitesFormationDao iUnitesFormationDao;
	
	@Autowired
	ISessionsServices iSessionServices;
	
//	@Autowired
//	IUnitesFormationServices iUFormationServices;
	
	@ApiOperation(value = "Récupère la liste des sessions.")
	@RequestMapping(value = "/Sessions", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeSessions() {

		List<Sessions> listeSessions = iSessionDao.findAll();
		MappingJacksonValue mjvListeSessions = new MappingJacksonValue(listeSessions);

		if (listeSessions.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeSessions);
		}
	}
	
	@ApiOperation(value = "Récupère une session via son ID.")
	@RequestMapping(value = "/Sessions/findid={id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUneSession(@PathVariable Long id) {

		Sessions sessionsFind = iSessionDao.findById(id).get();
		MappingJacksonValue mjvSessions = new MappingJacksonValue(sessionsFind);

		if (sessionsFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvSessions);
		}
	}
	
	@ApiOperation(value = "Récupère une session via un string \"***\".")
	@RequestMapping(value = "/Sessions/finddatap", method = RequestMethod.POST)
	public ResponseEntity<Object> afficherUneSessionByDataP(@RequestBody String data) {

		Sessions sessionFind = iSessionServices.findByDataSession(data);
		MappingJacksonValue mjvSession = new MappingJacksonValue(sessionFind);

		if (sessionFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvSession);
		}
	}
	
	@ApiOperation(value = "Récupère une session via l'ID d'un formateur.")
	@RequestMapping(value = "/Sessions/form={id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherSessionsForm(@PathVariable Long id) {

		List<Sessions> sessionsAnimFind = iSessionServices.listAnimationsSessionsByPers(id);
		MappingJacksonValue mjvSessions = new MappingJacksonValue(sessionsAnimFind);

		if (sessionsAnimFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvSessions);
		}
	}
	
	@ApiOperation(value = "Récupère une session via l'ID d'un stagiaire.")
	@RequestMapping(value = "/Sessions/stag={id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherSessionsStag(@PathVariable Long id) {

		List<Sessions> sessionsPartFind = iSessionServices.listParticipationsSessionsByPers(id);
		MappingJacksonValue mjvSessions = new MappingJacksonValue(sessionsPartFind);

		if (sessionsPartFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvSessions);
		}
	}
	
	@ApiOperation(value = "Ajouter une session.")
//	@ApiIgnore
	@PostMapping(value = "/Sessions")
	public ResponseEntity<Object> addSession(@RequestBody Sessions session) {
		List<UnitesFormation> listUnitesFormation = iUnitesFormationDao.findBySession(session);
		for (UnitesFormation uFormation : listUnitesFormation) {
			uFormation.setSession(null);
		}
		List<UnitesFormation> listUnitesFormationSession = session.getListUnitesFormation();
		UnitesFormation uniteFormationAdd = new UnitesFormation();
		for (UnitesFormation uFormation : listUnitesFormationSession) {
			uFormation.setSession(session);
			uniteFormationAdd = iUnitesFormationDao.save(uFormation);
			listUnitesFormation.add(uniteFormationAdd);
		}	

		session.setListUnitesFormation(listUnitesFormation);
		Sessions sessionAdd = iSessionDao.save(session);

		if (sessionAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(sessionAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprimer une session via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Sessions/delid={id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerSessions(@PathVariable Long id) throws NotFoundException {
		
		iSessionDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}

	@ApiOperation(value = "Récupère une liste des libellés des sessions.")
//	@ApiIgnore
	@RequestMapping(value = "/Sessions/listlibsess", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherLibelleSession() {

		List<String> listLibelleSession = iSessionServices.libelleSessionAllSession();
		MappingJacksonValue mjvListLibelleSession = new MappingJacksonValue(listLibelleSession);

		if (listLibelleSession.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListLibelleSession);
		}
	}
}
