package fr.azzahrah.cfback.controler;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.crossstore.ChangeSetPersister.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import fr.azzahrah.cfback.beans.Categories;
import fr.azzahrah.cfback.idao.ICategoriesDao;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController // annotation Spring
@CrossOrigin("*")
@Api(description="API pour les opérations CRUD sur les Catégories.")
public class CategoriesController {

	@Autowired
	ICategoriesDao iCategorieDao;
	
	@ApiOperation(value = "Récupère la liste des catégories.")
	@RequestMapping(value = "/Categories", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherListeCategories() {

		List<Categories> listeCategories = iCategorieDao.findAll();
		MappingJacksonValue mjvListeCategories = new MappingJacksonValue(listeCategories);

		if (listeCategories.size() == 0) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvListeCategories);
		}
	}
	
	@ApiOperation(value = "Récupère une catégorie via son ID.")
	@RequestMapping(value = "/Categories/{id}", method = RequestMethod.GET)
	public ResponseEntity<Object> afficherUneCategorie(@PathVariable Long id) {

		Categories categoriesFind = iCategorieDao.findById(id).get();
		MappingJacksonValue mjvCategories = new MappingJacksonValue(categoriesFind);

		if (categoriesFind == null) {
			return ResponseEntity.noContent().build();
		} else {
			return ResponseEntity.ok().body(mjvCategories);
		}
	}
	
	@ApiOperation(value = "Ajoute une adresse.")
//	@ApiIgnore
	@PostMapping(value = "/Categories")
	public ResponseEntity<Object> ajouterCatalogue(@RequestBody Categories categorie) {
		Categories categorieAdd = iCategorieDao.save(categorie);

		if (categorieAdd == null)
			return ResponseEntity.noContent().build();
		URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
				.buildAndExpand(categorieAdd.getId()).toUri();

		return ResponseEntity.created(location).build();
	}
	
	@ApiOperation(value = "Supprime une adresse via son ID.")
//	@ApiIgnore
	@RequestMapping(value="/Categories/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> supprimerCategories(@PathVariable Long id) throws NotFoundException {
		
		iCategorieDao.deleteById(id);
		return ResponseEntity.noContent().build();
	}
	
	
}
