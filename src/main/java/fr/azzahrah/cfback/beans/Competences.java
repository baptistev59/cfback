package fr.azzahrah.cfback.beans;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Competences {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "competences_generator")
	@SequenceGenerator(name = "competences_generator", sequenceName = "competences_id_seq", allocationSize = 1)
	private Long id;
	private String libelleCompetence;
	@Column(length=2500)
	private String detailsCompetence;

	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_formations")
	@JsonIgnore
	private Formations formation;
}