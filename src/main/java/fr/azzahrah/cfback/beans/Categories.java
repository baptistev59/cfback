package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Categories {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "categories_generator")
	@SequenceGenerator(name = "categories_generator", sequenceName = "categories_id_seq", allocationSize = 1)
	private Long id;
	private String libelleCategorie;
	private String detailsCategorie;

	@OneToMany(mappedBy = "categorie")
	@JsonIgnore
	private List<Formations> listFormations;

}