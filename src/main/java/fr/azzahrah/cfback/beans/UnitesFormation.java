package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class UnitesFormation {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "unitesformations_generator")
	@SequenceGenerator(name = "unitesformations_generator", sequenceName = "unitesformations_id_seq", allocationSize = 1)
	public Long id;
	public String numUF;

	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name = "id_session")
	@JsonIgnore
	private Sessions session;

	@OneToMany(mappedBy = "uniteFormation")
	private List<Presences> listPresences;
	
	@ManyToOne(cascade=CascadeType.PERSIST)
	@JoinColumn(name="id_lieu")
	private Lieux lieu;
}