package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data

public class Formations {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "formations_generator")
	@SequenceGenerator(name = "formations_generator", sequenceName = "formations_id_seq", allocationSize = 1)
	private Long id;
	private String libelleFormation;
	private String detailsFormation;

	
	@OneToMany(mappedBy = "formation")
	@JsonIgnore
	private List<Sessions> listSessions;
	
	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_categories")
	private Categories categorie;
	
	@ManyToMany(mappedBy = "listFormations")
	@JsonIgnore
	private List<Catalogues> listCatalogues;
	
	@OneToMany(mappedBy = "formation")
	private List<Competences> listCompetences;
	
}