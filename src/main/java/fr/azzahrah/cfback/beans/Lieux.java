package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Lieux {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "lieux_generator")
	@SequenceGenerator(name = "lieux_generator", sequenceName = "lieux_id_seq", allocationSize = 1)
	private Long id;
	private String libelleLieu;
	private String detailsLieu;
	
	@OneToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name="id_adresse")
	private Adresses adresse;
	
	@OneToMany(mappedBy = "lieu")
	@JsonIgnore
	private List<Centre> listCentre;
	
	@OneToMany(mappedBy = "lieu")
	@JsonIgnore
	private List<UnitesFormation> listUF;
	

}