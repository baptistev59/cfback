package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Adresses {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "adresse_generator")
	@SequenceGenerator(name = "adresse_generator", sequenceName = "adresse_id_seq", allocationSize = 1)
	private Long id;
	private String housenumber;
	private String label;
	private String street;
	private String name;
	private String postcode;
	private Double lati;
	private Double longi;
	private String city;
	private String context;
	private String complement;
	
	@OneToMany(mappedBy = "adresse")
	@JsonIgnore
	private List<Lieux> listLieux;

}