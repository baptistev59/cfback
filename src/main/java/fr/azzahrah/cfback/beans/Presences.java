package fr.azzahrah.cfback.beans;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class Presences {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "presences_generator")
	@SequenceGenerator(name = "presences_generator", sequenceName = "presences_id_seq", allocationSize = 1)
	private Long id;
	private Boolean etatPresence;
	private Boolean signaturePresence;
	private Boolean validPresence;
	
	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_uniteformation")
	private UnitesFormation uniteFormation;
	
	@OneToOne
	@JoinColumn(name="id_formateur")
	private Personnes formateur;
	
	@OneToOne
	@JoinColumn(name="id_stagiaire")
	private Personnes stagiaire;
	
	

}