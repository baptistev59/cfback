package fr.azzahrah.cfback.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class Sessions {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sessions_generator")
	@SequenceGenerator(name = "sessions_generator", sequenceName = "sessions_id_seq", allocationSize = 1)
	private Long id;
	private LocalDate dateDebutSession;
	private LocalDate dateFinSession;
	private int nbUnitesFormation;
	private String libelleSession;

	@ManyToOne()
	@JoinColumn(name = "id_formation")
	private Formations formation;

	@OneToMany(mappedBy = "session")
	private List<UnitesFormation> listUnitesFormation;

	@ManyToMany(targetEntity = Personnes.class)
	private List<Personnes> listFormateurs;

	@ManyToMany(targetEntity = Personnes.class)
	private List<Personnes> listStagiaires;

}