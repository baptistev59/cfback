package fr.azzahrah.cfback.beans;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Entity
@Data
public class Personnes {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "personnes_generator")
	@SequenceGenerator(name = "personnes_generator", sequenceName = "personnes_id_seq", allocationSize = 1)
	private Long id;
	private String nomPers;
	private String prenomPers;
	private String mailPers;
	private LocalDate dateNaisPers;
	private String idDossPers;

	@ManyToMany (mappedBy = "listFormateurs")
	@JsonIgnore
	private List<Sessions> listAnimationsSessions;

	@ManyToMany (mappedBy = "listStagiaires")
	@JsonIgnore
	private List<Sessions> listParticipationsSessions;
	
	@ManyToMany
	private List<Roles> listRoles;
}