package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
public class Roles {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_generator")
	@SequenceGenerator(name = "roles_generator", sequenceName = "roles_id_seq", allocationSize = 1)
	private Long id;
	private String libeleRole;
	private String detailsRole;
	
	@ManyToMany (mappedBy = "listRoles")
	@JsonIgnore
	private List<Personnes> listPersonnes;
}
