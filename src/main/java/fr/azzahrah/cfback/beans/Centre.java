package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class Centre {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "centre_generator")
	@SequenceGenerator(name = "centre_generator", sequenceName = "centre_id_seq", allocationSize = 1)
	private Long id;

	private String libelle;
	private String slogan;
	
	private String tel;
	private String fax;
	private String email;
	private String site;
	private String siret;
	
	@OneToMany(mappedBy = "centre")
	private List<Users> listUsers;
	
	@ManyToOne(cascade = { CascadeType.PERSIST })
	@JoinColumn(name = "id_lieu")
	private Lieux lieu;
	
}
