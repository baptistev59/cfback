package fr.azzahrah.cfback.beans;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;

import lombok.Data;

@Entity
@Data
public class Catalogues {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "catalogues_generator")
	@SequenceGenerator(name = "catalogues_generator", sequenceName = "catalogues_id_seq", allocationSize = 1)
	private Long id;
	private String libelleCatalogue;
	private String detailsCatalogue;
	private boolean validite;
	
	@ManyToMany
	private List<Formations> listFormations;

}