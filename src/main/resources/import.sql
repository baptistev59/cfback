insert into categories (id, libelle_categorie) values (nextval('categories_id_seq'), 'Informatique')
commit;

insert into formations (id, libelle_formation, id_categories, details_formation) values (nextval('formations_id_seq'), 'Formation CDA', 1, 'Formation de Concepteur Développeur d''Application');
insert into formations (id, libelle_formation, id_categories, details_formation) values (nextval('formations_id_seq'), 'Formation Web', 1, 'Formation de Développeur d''Application Web');
insert into formations (id, libelle_formation, id_categories, details_formation) values (nextval('formations_id_seq'), 'Formation JavaScript', 1, 'Formation de Développeur JavaScript');
commit;

insert into catalogues (id, libelle_catalogue,validite) values (nextval('catalogues_id_seq'),'Catalogue 2018',false);
insert into catalogues (id, libelle_catalogue,validite) values (nextval('catalogues_id_seq'),'Catalogue 2019',false);
insert into catalogues (id, libelle_catalogue,validite) values (nextval('catalogues_id_seq'),'Catalogue 2020',true);
commit;

insert into catalogues_list_formations (list_catalogues_id, list_formations_id) values (3,1);
insert into catalogues_list_formations (list_catalogues_id, list_formations_id) values (3,2);
insert into catalogues_list_formations (list_catalogues_id, list_formations_id) values (3,3);
commit;

insert into roles (id, libele_role, details_role) values (nextval('roles_id_seq'),'Formateur','Animation de formations');
insert into roles (id, libele_role, details_role) values (nextval('roles_id_seq'),'Stagiaire','Particpation aux formations');
commit;

insert into adresses (id,postcode,label,name,housenumber,street, city) values (nextval('users_id_seq'), '59100','24 Voie Rapide Lille Roubaix Tourcoing 59100 Roubaix','24 Voie Rapide Lille Roubaix Tourcoing','24','Voie Rapide Lille Roubaix Tourcoing','Roubaix');

insert into lieux (id, libelle_lieu, details_lieu, id_adresse) values (nextval('lieux_id_seq'), 'Centre Mercure', 'Centre de formation Azzahrah - Centre mercure - 2ème étage',1);
insert into lieux (id, libelle_lieu, details_lieu, id_adresse) values (nextval('lieux_id_seq'), 'Salle 22', 'Centre de formation Azzahrah - Centre mercure - 2ème étage',1);
insert into lieux (id, libelle_lieu, details_lieu, id_adresse) values (nextval('lieux_id_seq'), 'Salle 23', 'Centre de formation Azzahrah - Centre mercure - 2ème étage',1);
commit;


insert into sessions (id, date_debut_session, date_fin_session,libelle_session,nb_unites_formation,id_formation) values (nextval('sessions_id_seq'),'2019-01-07','2019-11-29', 'Session CDA janvier 2019',250,1);
insert into sessions (id, date_debut_session, date_fin_session,libelle_session,nb_unites_formation,id_formation) values (nextval('sessions_id_seq'),'2019-05-07','2019-08-25', 'Session WEB janvier 2019',100,2);
commit;

insert into personnes (id,nom_pers,prenom_pers,mail_pers,date_nais_pers,id_doss_pers) values (nextval('personnes_id_seq'),'HASNI', 'Sofiane', 'sofiane@gmail.com', '1997-05-29', '20190917100535-14');
insert into personnes (id,nom_pers,prenom_pers,mail_pers,date_nais_pers,id_doss_pers) values (nextval('personnes_id_seq'),'BENJIRA', 'Mohammed', 'm.benjira@gmail.com', '1987-05-29', '20190917100322-12');
insert into personnes (id,nom_pers,prenom_pers,mail_pers,date_nais_pers,id_doss_pers) values (nextval('personnes_id_seq'),'VANDAELE', 'Baptiste', 'baptistev59@free.fr', '1976-10-08', '20190917100443-13');
commit;

insert into sessions_list_formateurs (list_animations_sessions_id,list_formateurs_id) values (1,2);
insert into sessions_list_formateurs (list_animations_sessions_id,list_formateurs_id) values (1,3);
insert into sessions_list_formateurs (list_animations_sessions_id,list_formateurs_id) values (2,2);
commit;

insert into sessions_list_stagiaires (list_participations_sessions_id,list_stagiaires_id) values (1,1);
insert into sessions_list_stagiaires (list_participations_sessions_id,list_stagiaires_id) values (2,3);
commit;

insert into unites_formation (id, numuf,id_session, id_lieu) values (nextval('unitesformations_id_seq'),'CDA-JAN19-001',1,1);
insert into unites_formation (id, numuf,id_session, id_lieu) values (nextval('unitesformations_id_seq'),'WEB-JAN19-001',2,2);
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-002');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-003');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-004');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-005');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-006');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-007');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-008');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-009');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'WEB-JAN19-010');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'CDA-JAN19-002');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'CDA-JAN19-003');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'CDA-JAN19-004');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'CDA-JAN19-005');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'CDA-JAN19-006');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'CDA-JAN19-007');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-001');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-002');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-003');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-004');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-005');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-006');
insert into unites_formation (id, numuf) values (nextval('unitesformations_id_seq'),'JAS-SEP19-007');

commit;

insert into competences (id, libelle_competence,details_competence,id_formations) values (nextval('competences_id_seq'),'Maquetter une application','À partir de cas d utilisation ou de scénarios utilisateur, de la charte graphique et des exigences de sécurité identifiées, concevoir la maquette des interfaces utilisateurs de l’application, avec du contenu en langue française ou anglaise, y compris celles appropriées à l’équipement ciblé et en tenant compte de l’expérience utilisateur et pour un équipement mobile des spécificités ergonomiques. Formaliser les enchaînements des interfaces afin que l’utilisateur les valide ainsi que les maquettes.','1');
insert into competences (id, libelle_competence,details_competence,id_formations) values (nextval('competences_id_seq'),'Développer une interface utilisateur de type desktop','À partir du dossier de conception technique contenant la maquette de l''interface utilisateur à développer et à l''aide d''un environnement de développement intégré, éventuellement de langue anglaise, et d''un langage orienté objet, coder, tester, documenter et installer les composants logiciels requis, formulaires et états, afin d’assurer la collecte et la restitution des informations numériques relatives aux besoins du métier de l''utilisateur. Respecter les bonnes pratiques de la programmation orientée objet et les règles du développement sécurisé. Rechercher, éventuellement en langue anglaise, des solutions pertinentes pour résoudre des problèmes techniques ou mettre en oeuvre de nouvelles fonctionnalités. Pratiquer une veille technologique sur la sécurité informatique et les vulnérabilités connues. Partager le résultat de sa recherche ou de sa veille avec ses pairs.','1');
insert into competences (id, libelle_competence,details_competence,id_formations) values (nextval('competences_id_seq'),'Développer des composants d’accès aux données','À partir du dossier de conception technique et d’une bibliothèque d’objets spécialisés dans l’accès aux données, coder, tester et documenter les composants d''accès aux données stockées dans une base de données afin d’opérer des sélections et des mises à jour de données nécessaires à une application informatique et de façon sécurisée. Pratiquer une veille technologique, y compris en anglais, pour résoudre un problème technique ou mettre en oeuvre une nouvelle fonctionnalité ainsi que pour s’informer sur la sécurité informatique et les vulnérabilités connues. Partager le résultat de sa veille avec ses pairs','1');
insert into competences (id, libelle_competence,details_competence,id_formations) values (nextval('competences_id_seq'),'Développer la partie front-end d’une interface utilisateur web','À partir du dossier de conception technique contenant la maquette de l''interface utilisateur à développer et à l’aide des langages de développement web, créer les interfaces utilisateur web (pages web), puis coder, tester et documenter les traitements côté client, afin d’obtenir un rendu visuel adapté à l’équipement utilisateur et de fluidifier l’expérience utilisateur. Prendre en compte les différents équipements et navigateurs ciblés. Respecter les bonnes pratiques de développement web, d''accessibilité et les règles du développement sécurisé. Pratiquer une veille technologique, y compris en anglais, pour résoudre un problème technique ou mettre en oeuvre une nouvelle fonctionnalité ainsi que pour s’informer sur la sécurité informatique et les vulnérabilités connues.Partager le résultat de sa veille avec ses pairs.','1');
commit;

insert into users (id,username,password,first_name,last_name,token) values (nextval('users_id_seq'),'admin','admin','Administrateur','Centre de formation','');
commit;


insert into centre (id, libelle, slogan, tel,fax,email,site,siret,id_lieu) values (nextval('centre_id_seq'), 'Association Azzahrah', 'FACILITER L’ECLOSION DE L’APPRENTISSAGE.','0320030303','0320030304','azzahrah@gmail.com','azzahrah.asso.fr','802 954 785 00028',1);
commit;