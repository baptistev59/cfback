package fr.azzahrah.cfback;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CfbackApplication {

	public static void main(String[] args) {
		SpringApplication.run(CfbackApplication.class, args);
	}

}
